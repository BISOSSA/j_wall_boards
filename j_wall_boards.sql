BEGIN
  dbms_scheduler.create_job(job_name        => 'j_wall_boards'
                           ,job_type        => 'STORED_PROCEDURE'
                           ,job_action      => 'dba_sosdw.p_llenar_red_nacional'
                           ,start_date      => '03/02/2020 11:00:00 -03:00'
                           ,repeat_interval => 'FREQ=SECONDLY;INTERVAL=30'
                           ,end_date        => NULL
                           ,auto_drop       => FALSE
                           ,enabled         => TRUE);
END;
